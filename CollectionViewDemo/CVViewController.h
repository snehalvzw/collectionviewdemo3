//
//  CVViewController.h
//  CollectionViewDemo1
//
//  Created by Marc Respass on 10/08/2012.
//  Copyright (c) 2012 ILIOS Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CVViewController : UICollectionViewController <UICollectionViewDelegateFlowLayout>

@end
