
// IsEmpty courtesy of Will Shipley
// http://www.wilshipley.com/blog/2005/10/pimp-my-code-interlude-free-code.html
static inline BOOL IsEmpty(id thing) {
    return thing == nil	|| ([thing respondsToSelector:@selector(length)] && [(id)thing length] == 0) || ([thing respondsToSelector:@selector(count)] && [(id)thing count] == 0);
}

// Enhancement by Wade Price
static inline BOOL IsNotEmpty(id thing) {
    return !IsEmpty(thing);
}

//#define RGB(r, g, b) 
//[UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
//#define RGBA(r, g, b, a) 
//[UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

// Logging

#define MERLogInfo(fmt, ...) NSLog(@"%s " fmt, __PRETTY_FUNCTION__, ##__VA_ARGS__)
#define MERLogWarning(fmt, ...) NSLog(@"%s Warning: " fmt, __PRETTY_FUNCTION__, ##__VA_ARGS__)
#define MERLogError(fmt, ...) NSLog(@"%s Error: " fmt, __PRETTY_FUNCTION__, ##__VA_ARGS__)

#ifdef DEBUG

    #define MERLogDebug(fmt, ...) NSLog(@"%s " fmt, __PRETTY_FUNCTION__, ##__VA_ARGS__)
    #define MERTrace() MERLogDebug(@"%s:%d", __FILE__, __LINE__)

#else

    #define MERLogDebug(...)
    #define MERTrace()

#endif

#ifdef DEBUG2

    #define MERLogDebug2(fmt, ...) NSLog(@"%s " fmt, __PRETTY_FUNCTION__, ##__VA_ARGS__)

#else

    #define MERLogDebug2(...)

#endif

