//
//  main.m
//  CollectionViewDemo1
//
//  Created by Marc Respass on 10/08/2012.
//  Copyright (c) 2012 ILIOS Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CVAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CVAppDelegate class]));
    }
}
