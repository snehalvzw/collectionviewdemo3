//
//  CVCustomCell.h
//  CollectionViewDemo2
//
//  Created by Marc Respass on 10/08/2012.
//  Copyright (c) 2012 ILIOS Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CVCustomCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
