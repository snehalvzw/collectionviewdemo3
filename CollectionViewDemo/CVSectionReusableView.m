//
//  CVHeaderReusableView.m
//  CollectionViewTest2
//
//  Created by Marc Respass on 10/03/2012.
//  Copyright (c) 2012 ILIOS Inc. All rights reserved.
//

#import "CVSectionReusableView.h"

@implementation CVSectionReusableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
