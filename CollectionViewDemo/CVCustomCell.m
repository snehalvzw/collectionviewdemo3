//
//  CVCustomCell.m
//  CollectionViewDemo2
//
//  Created by Marc Respass on 10/08/2012.
//  Copyright (c) 2012 ILIOS Inc. All rights reserved.
//

#import "CVCustomCell.h"

@implementation CVCustomCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
