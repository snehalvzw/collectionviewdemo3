//
//  CVAppDelegate.h
//  CollectionViewDemo1
//
//  Created by Marc Respass on 10/08/2012.
//  Copyright (c) 2012 ILIOS Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CVAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
