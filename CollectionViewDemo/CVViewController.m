//
//  CVViewController.m
//  CollectionViewDemo1
//
//  Created by Marc Respass on 10/08/2012.
//  Copyright (c) 2012 ILIOS Inc. All rights reserved.
//

#import "CVViewController.h"
#import "CVCustomCell.h"
#import "CVSectionReusableView.h"

#import "CommonMacros.h"

static NSInteger kNumberOfSections = 2;
static NSInteger kNumberOfItems = 4 * 7 * 2;

static CGFloat kChosenSection = 0;
static CGFloat kAvailableSection = 1;

static NSString *kCellIdentifier = @"DEMO_CELL";
static NSString *kSectionHeaderIdentifier = @"SECTION_VIEW";

static CGSize kStandardCellSize = {140.0, 120.0};
static CGSize kStandardHeaderSize = {300.0, 37.0};

@interface CVViewController ()

@property (nonatomic, strong) NSMutableArray *chosenItems;
@property (nonatomic, strong) NSMutableArray *items;

@end

@implementation CVViewController

- (id)initWithCollectionViewLayout:(UICollectionViewLayout *)layout;
{
    if([super initWithCollectionViewLayout:layout])
    {
        static NSString *names[8] = {@"adjustments", @"alien", @"atom", @"hydrant", @"switch", @"swords", @"tree", @"wheel"};
        
        _chosenItems = [NSMutableArray arrayWithCapacity:kNumberOfItems];
        _items = [NSMutableArray arrayWithCapacity:kNumberOfItems];
        
        NSInteger index = 0;
        
        for(NSUInteger item = 0; item < kNumberOfItems; item++)
        {
            if(index > 7)
                index = 0;
            
            NSString *imageName = names[index];
            NSString *label = [NSString stringWithFormat:@"(%@) %@", imageName, @(item + 1)];

            index++;
            
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:3];
            
            [dict setValue:imageName forKey:@"imageName"];
            [dict setValue:label forKey:@"label"];
            
            [_items addObject:dict];
        }

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.collectionView.allowsMultipleSelection = YES;
    
    // 2. Register NIB instead of class
    UINib *nib = nil;
    
    nib = [UINib nibWithNibName:NSStringFromClass([CVCustomCell class]) bundle:nil];
    [self.collectionView registerNib:nib forCellWithReuseIdentifier:kCellIdentifier];

    // REGISTER SUPPLEMENTARY VIEW
    nib = [UINib nibWithNibName:NSStringFromClass([CVSectionReusableView class]) bundle:nil];
    [self.collectionView registerNib:nib forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kSectionHeaderIdentifier];

    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    layout.minimumInteritemSpacing = 10.0;
    layout.minimumLineSpacing = 10.0;
    layout.itemSize = kStandardCellSize;
    layout.headerReferenceSize = kStandardHeaderSize;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Cell Configuration
- (void)configureChosenCell:(CVCustomCell *)cell;
{
    cell.backgroundColor = [UIColor greenColor];
    cell.label.textColor = [UIColor redColor];
}

- (void)configureSelectedCell:(CVCustomCell *)cell;
{
    cell.backgroundColor = [UIColor lightGrayColor];
    cell.label.textColor = [UIColor redColor];
}

- (void)configureUnselectedCell:(CVCustomCell *)cell;
{
    cell.backgroundColor = [UIColor whiteColor];
    cell.label.textColor = [UIColor blackColor];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView;
{
    return kNumberOfSections;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;
{
    if(section == kChosenSection)
        return [self.chosenItems count];
    
    return [self.items count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    CVCustomCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellIdentifier forIndexPath:indexPath];
    
    NSDictionary *itemData = nil;
    
    if(indexPath.section == kChosenSection)
    {
        [self configureChosenCell:cell];

        itemData = [self.chosenItems objectAtIndex:indexPath.item];
    }
    else
    {
        if(cell.isSelected)
            [self configureSelectedCell:cell];
        else
            [self configureUnselectedCell:cell];
        
        itemData = [self.items objectAtIndex:indexPath.item];
    }
    
    cell.imageView.image = [UIImage imageNamed:[itemData valueForKey:@"imageName"]];
    cell.label.text = [itemData valueForKey:@"label"];
    
    return cell;
}

// WILL NOT BE CALLED UNLESS YOU IMPLEMENT
// collectionView:layout:referenceSizeForHeaderInSection:
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    CVSectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kSectionHeaderIdentifier forIndexPath:indexPath];
    
    headerView.sectionLabel.text = (indexPath.section == kChosenSection ? @"Chosen" : @"Available");
    
    return headerView;
}

#pragma mark - Selection
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    BOOL select = YES;
    
    if(indexPath.section == kAvailableSection)
    {
        UICollectionViewCell *selectedCell = [collectionView cellForItemAtIndexPath:indexPath];
        select = !selectedCell.isSelected;
    }
    
    MERLogDebug(@"%d", select);
    return select;
}

// called when the user taps on an already-selected item in multi-select mode
// If you do not implement this method, the default return value is YES.
- (BOOL)collectionView:(UICollectionView *)collectionView shouldDeselectItemAtIndexPath:(NSIndexPath *)indexPath;
{    
    return NO;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    MERLogDebug(@"%@", indexPath);
    
    if(indexPath.section == kChosenSection)
    {
        NSDictionary *itemData = [self.chosenItems objectAtIndex:indexPath.item];
        NSIndexPath *itemIndexPath = [itemData valueForKey:@"selectedItemIndexPath"];
        
        CVCustomCell *cell = (CVCustomCell *)[collectionView cellForItemAtIndexPath:itemIndexPath];
//        [cell setSelected:NO]; // Does not work
        [collectionView deselectItemAtIndexPath:itemIndexPath animated:NO];
        
        [self configureUnselectedCell:cell];        
        
        [self.chosenItems removeObjectAtIndex:indexPath.item];
        [collectionView deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
    }
    else
    {
        CVCustomCell *cell = (CVCustomCell *)[collectionView cellForItemAtIndexPath:indexPath];
        [self configureSelectedCell:cell];
        
        NSMutableDictionary *itemData = [NSMutableDictionary dictionaryWithDictionary:[self.items objectAtIndex:indexPath.item]];
        [itemData setValue:indexPath forKey:@"selectedItemIndexPath"];
        [self.chosenItems addObject:itemData];
        
        [collectionView insertItemsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForItem:([self.chosenItems count] - 1) inSection:0]]];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    MERLogDebug(@"%@", indexPath);

    CVCustomCell *cell = (CVCustomCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [self configureUnselectedCell:cell];
}

#pragma mark - UICollectionViewDelegateFlowLayout
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section;
//{
//    return ([self.chosenItems count] > 0 ? kStandardHeaderSize : CGSizeZero);
//}

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;
//{
//    return kStandardCellSize;
//}

@end
