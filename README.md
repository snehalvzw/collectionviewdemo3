# CollectionViewDemo3

CollectionViewDemo3 builds on CollectionViewDemo2

CollectionViewDemo3 adds CVSectionReusableView which is used as a supplementary view which is registered via 

	-registerNib:forSupplementaryViewOfKind:withReuseIdentifier:

CollectionViewDemo3 implements to determine the size of the header (supplementary view) based on if self.chosenItems has any items

	-collectionView:layout:referenceSizeForHeaderInSection:


CollectionViewDemo2 demonstrate selecting and deselecting cells in a different way where the selected cells are disabled and added to an array which makes up the first section. Selecting a cell from the first section removes it, re-enabling it in the second section.



© 2012 by Marc Respass, ILIOS Inc.
https://github.com/marcrespass

